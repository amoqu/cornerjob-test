'use strict';

var
  path = require('path'),
  expect = require('chai').expect,
  marsController = require(path.resolve('modules/mars/controllers/mars-instructions.controller'));

describe('basicExamples', function () {
  it('Basic example exposed in Tests Backend', function () {
    var instructions = [
      '5 5',
      '1 2 N',
      'LMLMLMLMM',
      '3 3 E',
      'MMRMMRMRRM'
    ];
    var output = [
      '1 3 N',
      '5 1 E'
    ];
    expect(marsController.sendInstructions(instructions)).to.eql(output);
  });

  it('Example 2', function () {
    var instructions = [
      '5 5',
      '1 2 N',
      'LMLMLMLMMLMLMLMLMM',
      '3 3 E',
      'MMRMMRMRRM'
    ];
    var output = [
        '1 4 N',
        '5 1 E'
    ];
    expect(marsController.sendInstructions(instructions)).to.eql(output);
  });

  it('Example 3', function () {
    var instructions = [
      '6 5',
      '1 2 N',
      'LMLMLMLMMLMLMLMLMM',
      '3 3 E',
      'MMRMMRMRRM',
      '1 2 N',
      'LM',
      '3 3 E',
      'MMRMMRMRRM',
      '1 2 N',
      'LMLMLMLMMLMLMLMLMM',
      '3 3 E',
      'L'
    ];
    var output = [
      '1 4 N',
      '5 1 E',
      '0 2 W',
      '5 1 E',
      '1 4 N',
      '3 3 N'
    ];
    expect(marsController.sendInstructions(instructions)).to.eql(output);
  });
});

describe('unexpectedInput', function () {
  it('Empty input', function () {
    var instructions = [
    ];
    var output = [
    ];
    expect(marsController.sendInstructions(instructions)).to.eql(output);
  });
  it('Without actions', function () {
    var instructions = [
      '6 5',
      '1 2 N',
    ];
    var output = [
    ];
    expect(marsController.sendInstructions(instructions)).to.eql(output);
  });
  it('Only rotate actions', function () {
    var instructions = [
      '6 5',
      '1 2 N',
      'RRRR'
    ];
    var output = [
      '1 2 N'
    ];
    expect(marsController.sendInstructions(instructions)).to.eql(output);
  });
  it('Fail Rover initial point', function () {
    var instructions = [
      '1 1',
      '3 2 N',
    ];
    var output = [
    ];
    expect(marsController.sendInstructions(instructions)).to.eql(output);
  });
  it('Try go out plateau', function () {
    var instructions = [
      '2 2',
      '0 0 N',
      'MMMMM',
    ];
    var output = [
      '0 2 N'
    ];
    expect(marsController.sendInstructions(instructions)).to.eql(output);
  });
  it('Try go out plateau 2', function () {
    var instructions = [
      '2 2',
      '0 0 N',
      'MMMMMRRMMMMMMMMM',
    ];
    var output = [
      '0 0 S'
    ];
    expect(marsController.sendInstructions(instructions)).to.eql(output);
  });
  it('Unexpected char action', function () {
    var instructions = [
      '2 2',
      '0 0 N',
      'PTRMJT',
    ];
    var output = [
      '1 0 E'
    ];
    expect(marsController.sendInstructions(instructions)).to.eql(output);
  });
});