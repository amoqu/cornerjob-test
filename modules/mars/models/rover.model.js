'use strict';

var
  path = require('path'),
  CardinalDirections = require(path.resolve('modules/mars/constants/cardinal-directions.constant')),
  RoverActions = require(path.resolve('modules/mars/constants/rover-actions.constant'))
;

function Rover (plateau, x, y, cardinalDirection) {
  this.plateau = plateau;
  this.x = x;
  this.y = y;
  this.cardinalDirection = cardinalDirection;
}

Rover.prototype.isValid = function() {
  var self = this;
  return (
    (self.isPointValid(self.x, self.y)) &&
    (self.isCardinalDirectionValid())
  );
};

Rover.prototype.isPointValid = function(x, y) {
  var self = this;
  return (
    (x>=0) &&
    (x<=self.plateau.x) &&
    (y>=0) &&
    (y<=self.plateau.y)
  );
};

Rover.prototype.isCardinalDirectionValid = function() {
  var self = this;
  return (
    (self.cardinalDirection==CardinalDirections.north) ||
    (self.cardinalDirection==CardinalDirections.east) ||
    (self.cardinalDirection==CardinalDirections.south) ||
    (self.cardinalDirection==CardinalDirections.west)
  );
};

Rover.prototype.executeAction = function(action) {
  var self = this;
  switch (action) {
    case RoverActions.left:
        self.rotateLeft();
      break;
    case RoverActions.right:
        self.rotateRight();
      break;
    case (RoverActions.move):
        self.advance();
      break;
    default:
      //console.warn('Warning: Undefined action: '+action);
  }
};

Rover.prototype.advance = function() {
  var self = this,
    x = self.x,
    y = self.y;

  switch (self.cardinalDirection) {
    case CardinalDirections.north:
      y++;
      break;
    case CardinalDirections.east:
      x++;
      break;
    case CardinalDirections.south:
      y--;
      break;
    case CardinalDirections.west:
      x--;
      break;
  }

  if (self.isPointValid(x,y)) {
    self.x = x;
    self.y = y;
  } else {
    //console.warn('Warning: Rover advances out of plateau', self.x, x, self.y, y);
  }
};

Rover.prototype.rotateLeft = function() {
  var self = this;

  switch (self.cardinalDirection) {
    case CardinalDirections.north:
      self.cardinalDirection = CardinalDirections.west;
      break;
    case CardinalDirections.east:
      self.cardinalDirection = CardinalDirections.north;
      break;
    case CardinalDirections.south:
      self.cardinalDirection = CardinalDirections.east;
      break;
    case CardinalDirections.west:
     self.cardinalDirection = CardinalDirections.south;
      break;
  }
};

Rover.prototype.rotateRight = function() {
  var self = this;

  switch (self.cardinalDirection) {
    case CardinalDirections.north:
      self.cardinalDirection = CardinalDirections.east;
      break;
    case CardinalDirections.east:
      self.cardinalDirection = CardinalDirections.south;
      break;
    case CardinalDirections.south:
      self.cardinalDirection = CardinalDirections.west;
      break;
    case CardinalDirections.west:
     self.cardinalDirection = CardinalDirections.north;
      break;
  }
};

module.exports = Rover;