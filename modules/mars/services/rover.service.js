'use strict';

var
  path = require('path'),
  Rover = require(path.resolve('modules/mars/models/rover.model'))
;

/**
 * [launchRover description]
 * @param  {Plateau} plateau          Object Plateau
 * @param  {number} x                 x point
 * @param  {number} y                 y point
 * @param  {char} cardinalDirection   cardinalDirection: see cardinal-directions.constant
 * @param  {string} actions           string of actions: see rover-actions.constant
 * @return {Rover}                    Object Rover if points and cardinalDirection are correct, if not return -1
 */
exports.launchRover = function(plateau, x, y, cardinalDirection, actions) {
    var
        rover,
        i;
    rover = new Rover(plateau, x, y, cardinalDirection);
    if (!rover.isValid()) return -1;

    for (i=0; i<actions.length; i++) {
        rover.executeAction(actions[i]);
    }
    return rover;
};
