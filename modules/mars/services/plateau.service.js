'use strict';

var
  path = require('path'),
  Plateau = require(path.resolve('modules/mars/models/plateau.model'))
;

/**
 * [initPlateau description]
 * @param  {number} x    x point
 * @param  {number} y    y point
 * @return {Plateau}     Object Plateau if points are correct, if not return -1
 */
exports.createPlateau = function(x, y) {
    var plateau;

    if ((x>=0)&&(y>=0)) {
        plateau = new Plateau(x, y);
    } else {
        return -1;
    }

    return plateau;
};
