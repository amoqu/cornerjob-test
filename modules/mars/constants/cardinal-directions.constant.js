'use strict';

var CardinalDirections = {
  north: 'N',
  east: 'E',
  south: 'S',
  west: 'W'
};

module.exports = CardinalDirections;