'use strict';

var RoverActions = {
  left: 'L',
  right: 'R',
  move: 'M'
};

module.exports = RoverActions;