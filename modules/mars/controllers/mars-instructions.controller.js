'use strict';

var
  path = require('path'),
  plateauService = require(path.resolve('modules/mars/services/plateau.service')),
  roverService = require(path.resolve('modules/mars/services/rover.service'))
;

var sendPlateauInstructions = function(instruction) {
    var
        plateauMaxsArray,
        plateau;

    if (instruction) {
        plateauMaxsArray = instruction.split(' ');
        if (plateauMaxsArray.length===2) {
            plateau = plateauService.createPlateau(
                plateauMaxsArray.shift(),
                plateauMaxsArray.shift()
            );
        }
    }

    return plateau;
};

var sendRoverInstructions = function(instructions, plateau) {
    var
        rover,
        initPositionArray,
        actions,
        outputArray = [],
        i;

    for (i = 0; i+1 < instructions.length; i=i+2) {
        initPositionArray = instructions[i].split(' ');
        actions = instructions[i+1];
        if (initPositionArray.length===3) {
            rover = roverService.launchRover(
                plateau,
                initPositionArray[0],
                initPositionArray[1],
                initPositionArray[2],
                actions
            );
            if (rover) {
                outputArray.push(rover.x+' '+rover.y+' '+rover.cardinalDirection);
            }
        }
    }
    return outputArray;
};

/**
 * Receives an instructions array and execute
 * @param  {Array} with instructions lines
 * @return {Array} with rovers response lines
 */
exports.sendInstructions = function(instructions) {
    var
        plateau,
        outputArray = [];

    plateau = sendPlateauInstructions(instructions.shift());
    if (plateau) {
        outputArray = sendRoverInstructions(instructions, plateau);
    }

    return outputArray;
};
