## Prerequisites

* Install Node.js - [Download & Install Node.js](https://nodejs.org/en/download/) and the npm package manager. If you encounter any problems, you can also use this [GitHub Gist](https://gist.github.com/isaacs/579814) to install Node.js.

## QUICK START
* Clone repository
```bash
$ git clone https://bitbucket.org/amoqu/cornerjob-test.git
```

* Install dependences with npm
```bash
$ npm install
```

## RUN

* Running basic example with npm
```bash
$ npm start
```

* Running test with npm
```bash
$ npm test
```

* Running other inputs by file with grunt (grunt readOrder:projectRelativePath)
```bash
$ grunt readOrder:resources/orders/order1
$ grunt readOrder:resources/orders/order2
$ grunt readOrder:resources/orders/order3
```