'use strict';

var
  path = require('path'),
  fs = require('fs'),
  marsController = require(path.resolve('modules/mars/controllers/mars-instructions.controller'));

module.exports = function(grunt) {

  // Add the grunt-mocha-test tassks.
  grunt.loadNpmTasks('grunt-mocha-test');

  grunt.initConfig({
    // Configure a mochaTest task
    mochaTest: {
      test: {
        options: {
          reporter: 'spec',
          quiet: false, // Optionally suppress output to standard out (defaults to false)
          clearRequireCache: false, // Optionally clear the require cache before running tests (defaults to false)
          noFail: false // Optionally set to not fail on failed tests (will still fail on other errors)
        },
        src: ['modules/**/tests/*.js']
      }
    }
  });

  grunt.registerTask('default', 'mochaTest');

  grunt.registerTask('readOrder', function(orderFile) {

    var filePath = path.resolve(orderFile);

    if (fs.existsSync(filePath)) {
      var lines = fs.readFileSync(filePath).toString().split('\n');
      var arrayOutput = marsController.sendInstructions(lines);
      if (arrayOutput) {
        fs.writeFileSync(filePath+'_output', arrayOutput.join('\n'));
        console.log(arrayOutput.join('\n'));
      }
    } else {
      console.log('File '+filePath+' doesn\'t exist');
    }
  });

};